package com.test.controllers;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.AbstractInputStreamContent;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.InputStreamContent;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.Drive.Files;
import com.google.api.services.drive.Drive.Files.Update;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.google.api.services.drive.model.Revision;
import com.google.api.services.drive.model.RevisionList;
import com.test.model.VersionFiles;
import com.test.model.VersionFilesImpl;

@Controller
public class DocumentController {
	
	 private static final String APPLICATION_NAME = "Google Drive API Java TestApp";
	 private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
	 private static final java.io.File CREDENTIALS_FOLDER = new java.io.File(System.getProperty("user.home"), "credentials");
	 private static final String CLIENT_SECRET_FILE_NAME = "client_secret.json";
	 private static final List<String> SCOPES = Collections.singletonList(DriveScopes.DRIVE);
	 private static NetHttpTransport HTTP_TRANSPORT;
	 private static Drive service;
	 private static File myFolder;
	 
	 @Autowired
	 VersionFilesImpl versionService;
	 
		
	@RequestMapping(value="/home", method=RequestMethod.GET)
	public ModelAndView viewHomePage(HttpServletRequest request, HttpServletResponse response){
		
		ModelAndView mav = new ModelAndView("home");
		
		try{
			googleDriveConnect();
		    HttpSession httpsession = request.getSession();
            String docToOpen = request.getParameter("param1");
            String revisionToOpen = request.getParameter("param2");
          
           	List<VersionFiles> versionHistory = versionService.getVersionHistory();
    		
    		//OPEN DOCUMENT
    		if(null!=revisionToOpen){
    			String docContent = viewDocument(docToOpen,revisionToOpen);
    			httpsession.setAttribute("docContent", docContent);
    			httpsession.setAttribute("versionToOpen", revisionToOpen);
    			httpsession.setAttribute("docToOpen",docToOpen);
    		}
    		//VIEW HOME PAGE
    		else{
    				httpsession.setAttribute("docContent", "");
    				httpsession.setAttribute("versionToOpen", "");
    				httpsession.setAttribute("docToOpen", "");
    		}
    		request.setAttribute("versionHistory",versionHistory);
    				
		}catch(Exception e)	{}
		//mav.addObject("home", new Document());
		return mav;
		
 }
	
	@RequestMapping(value="/home", method=RequestMethod.POST)
	public ModelAndView uploadDocument(HttpServletRequest request, HttpServletResponse response) throws Exception{
		
		googleDriveConnect();
		ModelAndView mav = null;
		java.io.File uploadFile = null;
		VersionFiles versionfile = new VersionFiles();
		HttpSession httpsession = request.getSession();
		
		String versionToOpen = request.getParameter("param1");
		String content = request.getParameter("mydoc");
		String docName = null;
		
		if(null!=request.getParameter("reset")){
			httpsession.setAttribute("docContent", "");
			httpsession.setAttribute("docToOpen", "");
		}if(null!=request.getParameter("save")){
						
			if(null!=content && !content.equals("")){
				String versionType = "Original";
				FileOutputStream out =null;
				docName = request.getParameter("docName");
				if(null == docName || docName.equals("")){
		    	  docName = (String) httpsession.getAttribute("docToOpen");
		    	  versionType = "Current";
				}
				if(!(docName.endsWith(".docx"))){
		    		  System.out.println(" No .docx");
		    		  docName = docName+".docx";
				}
				System.out.println(" Uploading Document "+docName);
			
				String fileName = "D:\\Sanila\\CrackVerbal\\"+docName;
				XWPFDocument document = new XWPFDocument(); 
				out = new FileOutputStream(new java.io.File(fileName));
				XWPFParagraph paragraph = document.createParagraph();
				XWPFRun run = paragraph.createRun();
				run.setText(content);
				XWPFRun paragraphOneRunThree = paragraph.createRun();
				paragraphOneRunThree.setFontSize(30);
				document.write(out);
				out.close();
				uploadFile = new java.io.File(fileName);
			
				//Create folder Documento in Google Drive if not exists
				String documentoId = getFolderId("Documento");
				System.out.println("Folder Documento Id is "+documentoId);
			
				if(null == documentoId){
					File newFolder = new File();
					newFolder.setName("Documento");
					newFolder.setMimeType("application/vnd.google-apps.folder");
					myFolder = service.files().create(newFolder).setFields("id").execute();
					System.out.println("Created Folder Documento...");
					System.out.println("Folder Documento Id is "+myFolder.getId());
					documentoId = myFolder.getId();
				}
			
				Revision googleFile = createGoogleFile(documentoId, "application/vnd.openxmlformats-officedocument.wordprocessingml.document", docName, uploadFile);
				
				if (uploadFile.exists()) {
					uploadFile.delete();
   			    	System.out.println(uploadFile.getName() + " is deleted!");
       	        } else {
       	            System.out.println("Delete operation is failed.");
       	        }
				 
				versionfile.setVersion(versionType);
				versionfile.setRevision_id(googleFile.getId());
				versionfile.setDocument(docName);
				versionfile.setCreatedDate(Calendar.getInstance().getTime());
				versionService.saveVersionHistory(versionfile);
			}else{
				httpsession.setAttribute("message", "Please enter Document name and Document content");
			}
		}
		List<VersionFiles> versionHistory = versionService.getVersionHistory();
		request.setAttribute("versionHistory",versionHistory);
		httpsession.setAttribute("docContent", "");
		httpsession.setAttribute("versionToOpen", "");
		httpsession.setAttribute("docToOpen", "");
		
		mav = new ModelAndView("home");
	    return mav;
	}
	
	public void googleDriveConnect()throws Exception{

		// 1: Create CREDENTIALS_FOLDER
    	if (!CREDENTIALS_FOLDER.exists()) {
    		 CREDENTIALS_FOLDER.mkdirs();
        	}
    	     
    	HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
    	
       //Read client_secret.json file & create Credential object.
        Credential credential = getCredentials(HTTP_TRANSPORT);
 
        //Create Google Drive Service.
        service = new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential) //
                .setApplicationName(APPLICATION_NAME).build();
        
        
 
   }

	public Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
	    java.io.File clientSecretFilePath = new java.io.File(CREDENTIALS_FOLDER, CLIENT_SECRET_FILE_NAME);
	    if (!clientSecretFilePath.exists()) {
	    	throw new FileNotFoundException("Please copy " + CLIENT_SECRET_FILE_NAME //
                + " to folder: " + CREDENTIALS_FOLDER.getAbsolutePath());
	    }

	    // Load client secrets.
	    InputStream in = new FileInputStream(clientSecretFilePath);
	    GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

	    // Build flow and trigger user authorization request.
	    GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY,
            clientSecrets, SCOPES).setDataStoreFactory(new FileDataStoreFactory(CREDENTIALS_FOLDER))
                    .setAccessType("offline").build();

	    return new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
	}
	
	 // Create Google File from java.io.File
    public static Revision createGoogleFile(String googleFolderIdParent, String contentType, String customFileName, java.io.File uploadFile) throws IOException {
         AbstractInputStreamContent uploadStreamContent = new FileContent(contentType, uploadFile);
         return _createGoogleFile(googleFolderIdParent, contentType, customFileName, uploadStreamContent);
    }
    
    private static Revision _createGoogleFile(String googleFolderIdParent, String contentType, String customFileName, AbstractInputStreamContent uploadStreamContent) throws IOException {
    	File fileMetadata = new File();
        File file = new File();
        //Drive.Files.Create createRequest;
        
        String fileId = checkIfFileExistsInFolder(customFileName);
        Revision revision = null;
        if(null == fileId){
        	fileMetadata.setName(customFileName);
            List<String> parents = Arrays.asList(googleFolderIdParent);
            fileMetadata.setParents(parents);
        	file = service.files().create(fileMetadata, uploadStreamContent).execute();
        	String newFileId = file.getId();
        	try {
                RevisionList revisions = service.revisions().list(newFileId).execute();
                List<Revision> revisionList = revisions.getRevisions();

                for(Revision newRevision : revisionList) {
                    revision = service.revisions().get(newFileId, newRevision.getId()).execute();
                    revision.setPublished(true);
                  
                }
             } catch (IOException e) {
                 System.out.println("An error occured: " + e);
             }
         }
        else{
        	//Changes
        	com.google.api.services.drive.model.File newContent = new File();
        	//newContent.setTrashed(true);
        	//List<String> parents = Arrays.asList(googleFolderIdParent);
        	//newContent.setParents(parents);
        	//service.files().update(fileId, newContent).execute();
        	//Changes -END
        	//file = service.files().update(fileId, newContent, uploadStreamContent).execute();
        	file = service.files().update(fileId, newContent, uploadStreamContent).setAddParents(googleFolderIdParent).setFields("id, parents").execute();
        	try {
        		
                RevisionList revisions = service.revisions().list(fileId).execute();
                List<Revision> revisionList = revisions.getRevisions();

                for(Revision newRevision : revisionList) {
                    revision = service.revisions().get(fileId, newRevision.getId()).execute();
                    revision.setPublished(true);
                   
                }
             } catch (IOException e) {
                 System.out.println("An error occured: " + e);
             }
        }
        return revision;
    }
    
    private static String getFolderId(String foldername) throws IOException{
    	String fileId = null;
    	FileList result = service.files().list().setPageSize(10).setFields("nextPageToken, files(id, name)").execute();
        List<File> files = result.getFiles();
        if (files == null || files.isEmpty()) {
            System.out.println("No files found.");
        } else {
           for (File file : files) {
        	   //System.out.println(" File Name :"+file.getName());
                if(file.getName().equals(foldername)){
                	 	fileId = file.getId();
                }
            }  
        }
        return fileId;
    }
    
    private static String checkIfFileExistsInFolder(String docname) throws IOException{
    	String fileId = null;
    	List<File> result = new ArrayList<File>();
        Files.List request = null;
    	//FileList result = service.files().list().setPageSize(10).setFields("nextPageToken, files(id, name)").execute();
    	try {
    		String documentoId = getFolderId("Documento");
            request = service.files().list();//plz replace your FOLDER ID in below linez
            FileList files = service.files().list().setQ("'"+documentoId+"' in parents").execute();
            result.addAll(files.getFiles());          
            request.setPageToken(files.getNextPageToken());
            for(File f:result){
            	if(docname.equals(f.getName())){
            		fileId = f.getId();
                }
             }
          } 
        catch (IOException e)
        {
          System.out.println("An error occurred: " + e);
          request.setPageToken(null);
        }
        return fileId;
    }
    
    
    private String viewDocument(String docname, String revisionToOpen) {
		// TODO Auto-generated method stub
			
		String words = "";
		FileOutputStream openedFile;
		FileInputStream fis;
		java.io.File  f = null;
		Revision revision = null;
		if(null!=revisionToOpen){
			try{
				//FileList result = service.files().list().setPageSize(10).setFields("nextPageToken, files(id, name)").execute();
				
				String documentoId = getFolderId("Documento");
				System.out.println("********* IN  viewDocument ********** documentoId is "+documentoId);
				        	
		        //String fileIdToOpen = checkIfFileExists(docname);
		        String fileIdToOpen = checkIfFileExistsInFolder(docname);
		        
		        if(null!=fileIdToOpen && !fileIdToOpen.equals("")){
	        		
		        RevisionList revisions = service.revisions().list(fileIdToOpen).execute();
		        List<Revision> revisionList = revisions.getRevisions();

		        	for(Revision newRevision : revisionList) {
	                 
		        		revision = service.revisions().get(fileIdToOpen, newRevision.getId()).execute();
		        		String revisionId = revision.getId();
		        		System.out.println("==============> Revision ID: " +revisionId );
	                 	        			
		        		if(null!= revisionId && !revisionId.equals("") && revisionId.equals(revisionToOpen)){

			        		String filename ="D:\\Sanila\\CrackVerbal\\FromRepo"+revisionToOpen+".docx";
			        		f = new java.io.File(filename); 
			        		openedFile =new FileOutputStream(f);
		        			service.revisions().get(fileIdToOpen, revisionToOpen).executeMediaAndDownloadTo(openedFile);
		        			fis = new FileInputStream(filename);
		       			    XWPFDocument doc = new XWPFDocument(fis);
		       			    List<XWPFParagraph> paras = doc.getParagraphs(); //This list will hold the paragraphs
		       			    XWPFWordExtractor ex = new XWPFWordExtractor(doc);  //To get the words
		       			    for(XWPFParagraph p : paras){  //For each paragraph we retrieved from the document
		       			      words += p.getText();    //Add the text we retrieve to the words string  
		       			    }
		       		        XWPFDocument newDoc = new XWPFDocument(); 
		       			    XWPFParagraph para = newDoc.createParagraph();
		       			    XWPFRun run = para.createRun();     
		       			    run.setText(words);
		       			    newDoc.write(new FileOutputStream(new java.io.File("D:\\Sanila\\CrackVerbal\\temp.docx")));
		       			    openedFile.close();
		       			    
		       			    java.io.File tempFile = new java.io.File("D:/Sanila/CrackVerbal/temp.docx");
		       			    if (tempFile.exists()) {
		       			    	tempFile.delete();
		       			    	System.out.println(tempFile.getName() + " is deleted!");
			       	        } else {
			       	            System.out.println("Delete operation is failed.");
			       	        }
		       			    
		       			   String RepoFileName = "D:/Sanila/CrackVerbal/FromRepo"+revisionToOpen+".docx";
		       			   System.out.println("RepoFileName is "+RepoFileName);
		       			    java.io.File RepoFile = new java.io.File(RepoFileName);
		       			    if (RepoFile.exists()) {
		       			    	RepoFile.delete();
		       			    	System.out.println(RepoFile.getName() + " is deleted!");
			       	        } else {
			       	            System.out.println("Delete operation is failed.");
			       	        }
		       			 System.out.println("RepoFile exists "+RepoFile.exists());
		                    break;
		        		}
		        	}
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
	}
			
	return words;

	}
}